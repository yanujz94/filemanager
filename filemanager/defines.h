#ifndef DEFINES_H
#define DEFINES_H
//----------DEBUG----------//
#define DEBUG 1
//----------DEBUG----------//


//----------AT COMMAND----------//
#define ENTER_COMMAND_MODE "+++"
#define EXIT_COMMAND_MODE "---"
#define WRITE_FILE_COMMAND "write_file"
#define READ_FILE_COMMAND "read_file"
#define CHANGE_DIR_COMMAND "cd"
#define MAKE_DIR_COMMAND "mkdir"

//----------AT COMMAND----------//
//----------SERIAL----------//
#define MAX_COMMAND_LENGTH 256
#define SPACE_DELIMITER " "
//----------SERIAL----------//


//----------MACROS----------//

#define printfln(x) printf("%s\r\n",x)
//----------MACROS----------//


#endif // DEFINES_H
