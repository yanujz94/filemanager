#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H
#include "Arduino.h"
#include "filemanager.h"
#include "defines.h"

enum Commands{
	//----------FILE OPERATIONS----------//
write_file,read_file,cd,mkdir,ls

};

struct _read_file{
	char *filename	=	nullptr;
};
struct _create_file{
	char *filename	=	nullptr;
	char *data		=	nullptr;
	char *mode		=	nullptr;
};

struct _mkdir{
	char *dirName	=	nullptr;
};
struct _cd{
	char *pathName	=	nullptr;
};


class CommandManager
{
public:
	CommandManager();


	void			performTask();
	Commands		mainCommand;
	_cd				cd;
	_mkdir			mkdir;
	_create_file	create_file;
	_read_file		read_file;
};

#endif // COMMANDMANAGER_H
