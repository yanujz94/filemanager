#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H
#include "defines.h"
#include "Arduino.h"
#include "commandmanager.h"

#include <string.h>
extern "C"{
#include	"osapi.h"
#include	"user_interface.h"
#include	"ets_sys.h"
#include	"c_types.h"

}
class SerialManager
{



public:
	SerialManager(uint baudrate);
	void serialRead(char terminator);
	char command[MAX_COMMAND_LENGTH];

private:
	void parseArgument();
	int getTokenNumber();
};

#endif // SERIALMANAGER_H
