#include "filemanager.h"

FileManager::FileManager()
{
	SPIFFS.begin();
}

void FileManager::write(const char *filename, const char *data, const char *mode)
{
	File f = SPIFFS.open(filename,mode);
	if(!f){
		printf("Can't open file!\r\n");
		return;
	}
	if(f.println(data)){
		printf("Data write!\r\n");
	}
	f.close();

}

void FileManager::read(const char *filename)
{
	File f = SPIFFS.open(filename,"r");
	while(f.available()){
		printf("%c",f.read());

	}

}
