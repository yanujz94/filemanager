#include "serialmanager.h"

SerialManager::SerialManager(uint baudrate)
{
	Serial.begin(baudrate);
	os_memset(command,0,MAX_COMMAND_LENGTH);
}

void SerialManager::serialRead(char terminator)
{
	char temp;

	static uint8_t index=0;
	while (Serial.available()) {
		temp= Serial.read();

		if(temp==terminator){
			command[index]='\0';
			index=0;
			printf("\r\n");

			parseArgument();


			return;
		}
		printf("%c",temp);

		command[index]=temp;


		index++;

	}
}

void SerialManager::parseArgument()
{	CommandManager *commandManager = new CommandManager;
	char *token;
	int i=0;
	char *arguments[20];

	token = strtok(command, SPACE_DELIMITER);


	if(strncmp(token,WRITE_FILE_COMMAND,strlen(WRITE_FILE_COMMAND))==0){
		commandManager->mainCommand=Commands::write_file;
		while( token != NULL ) {
			arguments[i]=token;
			i++;

			token = strtok(NULL, SPACE_DELIMITER);
		}
#if DEBUG
		printf("Token : %s\r\n",arguments[3]);
#endif
		for(i=0;i<=sizeof(*arguments)+1;i++){

			if(strncmp(arguments[i],"-n",2)==0){
				commandManager->create_file.filename=arguments[i+1];
			}
			else if(strncmp(arguments[i],"-d",2)==0){
				commandManager->create_file.data=arguments[i+1];
			}
			else if(strncmp(arguments[i],"-m",2)==0){
				commandManager->create_file.mode=arguments[i+1];
			}

		}

	}
	else if(strncmp(token,READ_FILE_COMMAND,strlen(READ_FILE_COMMAND))==0){
		//FIX ME
		token = strtok(NULL, SPACE_DELIMITER);
		commandManager->mainCommand=Commands::read_file;
		commandManager->read_file.filename=token;


	}
	else if(strncmp(token,MAKE_DIR_COMMAND,strlen(MAKE_DIR_COMMAND))==0){
		//FIX ME
		token = strtok(NULL, SPACE_DELIMITER);
		commandManager->mainCommand=Commands::mkdir;
		commandManager->mkdir.dirName=token;

	}
	else if(strncmp(token,CHANGE_DIR_COMMAND,strlen(CHANGE_DIR_COMMAND))==0){
		//FIX ME
		token = strtok(NULL, SPACE_DELIMITER);
		commandManager->mainCommand=Commands::cd;
		commandManager->cd.pathName=token;
	}

	else{
		printf("Command not found!\r\n");
		return;
	}

	//printf("Main command is : %s\r\n",token);
	//
	//
	commandManager->performTask();
	//delete commandManager;

}

int SerialManager::getTokenNumber()
{//TODO Maybe it's useless
	int counter=0,i=0;
	for(i=0;i<strlen(command);i++){
		if(command[i] == ' ')
			counter++;
	}
	return counter+1;
}
