#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include "Arduino.h"
#include "FS.h"

class FileManager
{
public:
	FileManager();
	void write(const char *filename, const char *data,  const char *mode);
	void read(const char *filename);
};

#endif // FILEMANAGER_H
