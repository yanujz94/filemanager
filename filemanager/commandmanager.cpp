#include "commandmanager.h"

CommandManager::CommandManager()
{

}

void CommandManager::performTask()
{
	//printf("Main command : %d\r\n",mainCommand);
	FileManager *fileManager= new FileManager;
	switch (mainCommand) {
	case Commands::write_file:
		#if DEBUG
		printf("I've to create a file!\r\n");
		printf("Filename is : %s\r\n",create_file.filename);
		printf("Data is : %s\r\n",create_file.data);
		printf("Mode is : %s\r\n",create_file.mode);
		#endif
		fileManager->write(create_file.filename,create_file.data,create_file.mode);
		break;
	case Commands::read_file:
		#if DEBUG
		printf("I've to read a file!\r\n");
		printf("Filename is : %s\r\n",read_file.filename);
		#endif
		fileManager->read(read_file.filename);
		break;
	case Commands::mkdir:
		#if DEBUG
		printf("I've to create a directory!\r\n");
		printf("Directory name is %s\r\n",mkdir.dirName);
		#endif
		break;
	case Commands::cd:
		#if DEBUG
		printf("I've to change directory!\r\n");
		printf("Path name is %s\r\n",cd.pathName);
		#endif
		break;
	}


}
